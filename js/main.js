var prevC = {"x":0,"y":0},
    delta = {"x":0,"y":0},
    strokeWidth = 2,
    strokeColor = "#000",
    pathSize = 0,
    path, loop, delay = 1, stop,
    w = window,
    d = document,
    svg = d.getElementById("svg"),
    download = d.getElementById("download");

function start() {
  d.body.addEventListener('touchmove', function(e){
    e.preventDefault();
  }, false);
  svg.addEventListener('mousedown', function(e){
    if (e.button === 0) {
      startDraw(e);
    }
  }, true);
  svg.addEventListener('touchstart', function(e){
    e.preventDefault();
    startDraw(e);
  }, false);
  svg.addEventListener('mouseup', function(e){endDraw(e)}, false);
  svg.addEventListener('touchend', function(e){endDraw(e)}, false);
}

start()

function mousemove(e) {
  e.preventDefault();
  var c = getCoords(e);
  draw(c.x, c.y);
}

function startDraw(e) {
  var c = getCoords(e);
  path = d.createElementNS('http://www.w3.org/2000/svg', 'path')
  path.setAttribute("fill","none")
  path.setAttribute("stroke",strokeColor)
  path.setAttribute("stroke-width",strokeWidth)
  path.setAttribute("d","M"+c.x+" "+c.y)
  svg.appendChild(path)
  w.addEventListener('mousemove', mousemove, false);
  svg.addEventListener('touchmove', mousemove, false);
}

function endDraw(e) {
  var c = getCoords(e);
  w.removeEventListener('mousemove', mousemove, false);
  svg.removeEventListener('touchmove', mousemove, false);
  pathSize = []
  clearInterval(loop)
  updateUrl()
  stop = true;
}

var draw = function(x, y){
  clearTimeout(loop)
  var pathData = path.getAttribute("d");
  if (x !== prevC.x && y !== prevC.y) {
    pathData += " L"+x+" "+y
    path.setAttribute("d",pathData)
    pathSize++
    prevC.x = x
    prevC.y = y
  }
  if(!stop){
    loop = setTimeout(draw,delay)
  }
}

function getCoords(e) {
  var rect = svg.getBoundingClientRect()
  cx = ((e.touches)? e.touches[0].pageX : e.pageX) - rect.left
  cy = ((e.touches)? e.touches[0].pageY : e.pageY) - rect.top
  return {
    x: cx,
    y: cy
  };
}

function updateUrl(){
  var svgData = svg.outerHTML
  var svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"})
  var svgUrl = URL.createObjectURL(svgBlob)
  download.href = svgUrl
  download.download = "drawing.svg"
}
